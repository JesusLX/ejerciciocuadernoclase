Cuaderno de Clase
===================
Esta aplicación es para administrar el manejo de alumnados de una institución, permite registrarlos, llevar un sistema de incidencias diarias, contactar con ellos a través de email, etc.

Al comienzo de la aplicación se puede ver un listado de los alumnos que han sido registrados en la base de datos.

![Screenshot_1488381923.png](https://bitbucket.org/repo/RXL8Rg/images/407831466-Screenshot_1488381923.png)

Cuando se pulsa en alguno de los elementos se abre una activity para mandarle un correo electrónico al alumno anteriormente seleccionado

![Screenshot_1488383260.png](https://bitbucket.org/repo/RXL8Rg/images/2900404751-Screenshot_1488383260.png)

y se envía con el botón enviar.

Cuando se deja presionado sobre un alumno aparece un menú flotante con las opciones de Modificar o Borrar:

![Screenshot_1488382335.png](https://bitbucket.org/repo/RXL8Rg/images/169946093-Screenshot_1488382335.png)

Al darle a modificar se abre la activity de modificar donde aparecen los datos actuales del usuario y que si se modifican y se da a aceptar se guardaran en la base de datos.

![Screenshot_1488382672.png](https://bitbucket.org/repo/RXL8Rg/images/3856610473-Screenshot_1488382672.png)

y al darle a eliminar se eliminaria el usuario 
[IMPORTANTE: También se eliminarán todas las incidencias que haya a su nombre.]

En la Toolbar, en el menú sale la opción de Incidencias, que abre la activity de éstas

![Screenshot_1488383920.png](https://bitbucket.org/repo/RXL8Rg/images/4209575679-Screenshot_1488383920.png)

![Screenshot_1488384135.png](https://bitbucket.org/repo/RXL8Rg/images/2209645519-Screenshot_1488384135.png)

Aquí se muestran las incidencias creadas en el día actual.

En el botón flotante se puede crear una nueva incidencia.

![Screenshot_1488384297.png](https://bitbucket.org/repo/RXL8Rg/images/1155723902-Screenshot_1488384297.png)

En el primer Spinner puedes seleccionar el alumno al que va dirigida la incidencia

![Screenshot_1488384952.png](https://bitbucket.org/repo/RXL8Rg/images/2729087091-Screenshot_1488384952.png)

En el de faltas, el tipo de falta

![Screenshot_1488385029.png](https://bitbucket.org/repo/RXL8Rg/images/1373518708-Screenshot_1488385029.png)

En el de trabajo, la actitud de trabajo

![Screenshot_1488385070.png](https://bitbucket.org/repo/Ky7Gbp/images/4265442426-Screenshot_1488385070.png)

Y en la de actitud el cómo ha sido ésta

![Screenshot_1488385145.png](https://bitbucket.org/repo/RXL8Rg/images/1917413917-Screenshot_1488385145.png)

Y por último el comentario (si se quiere añadir) de la incidencia.